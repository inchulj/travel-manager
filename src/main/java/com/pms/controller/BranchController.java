package com.pms.controller;

import com.pms.model.Branch;
import com.pms.repository.BranchRepository;
import com.pms.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

//creating RestController
@RestController
//     /tourism/api/v1/branch/add-places Adds a new company
//        /tourism/api/v1/branch/update-tariff/{companyID} Update the tariff details for the given place and respective company
//        URL Exposed Purpose
//        /tourism/api/v1/admin/{criteria}/{criteriaValue} Searches for places based on provided criteria
@RequestMapping("/tourism/api/v1")
public class BranchController
{
    List<String> places = Arrays.asList("ANDAMAN", "THAILAND", "DUBAI", "SINGAPORE", "MALAYSIA");

    //autowired the BranchService class
    @Autowired
    BranchService branchService;
    @Autowired
    BranchRepository branchRepository;

    @GetMapping("/branch")
    private List<Branch> getAllBranch()
    {
    return branchService.getAllBranch();
    }

    @GetMapping(value="/admin/branchId", params="branchID")
    private Branch getBranch(@RequestParam("branchID") Integer branchID)
    {
        return branchService.getBranchById(branchID);
    }

    @GetMapping(value="/admin/branchName", params="branchName")
    private ResponseEntity<List<Branch>> getBranchByBranchName(@RequestParam("branchName") String branchName)
    {
        ResponseEntity<List<Branch>> nl = new ResponseEntity<List<Branch>>(branchRepository.findByName(branchName), HttpStatus.OK);
        if (nl.getBody().isEmpty()) System.out.println("Branches not found");
        return nl;
    }

    @PostMapping("/branch/add-places")
    private int saveBranch(@RequestBody Branch branch) throws Exception
    {
        Map<String, Integer> tariff = branch.getTariff();
        Iterator iterator = tariff.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry element = (Map.Entry)iterator.next();
            if (!places.contains(element.getKey().toString().toUpperCase())) {
                System.out.println("place not applicable: "+ element.getKey());
                throw new Exception();
            }
            if (((int)element.getValue()) <5000 || ((int)element.getValue()) > 10000) {
                System.out.println("tariff out of range: "+ element.getValue());
                throw new Exception();
            }
        }
        Branch nb = new Branch();
        nb = branch;
        branchService.save(nb);
        return branch.getId();
    }

    @PostMapping("/branch/update-tariff/{branchID}")
    private int saveBranch(@PathVariable("branchID") int id,
                           @RequestParam("place") String place,
                           @RequestParam("tariff") int tariffAmout)
    {
        Branch nb = branchService.getBranchById(id);
        nb.getTariff().put(place, tariffAmout);
        branchService.update(id, nb);
        return id;
    }
}
