package com.pms.controller;

import com.pms.model.Policy;
import com.pms.model.PolicyRequest;
import com.pms.repository.PolicyRepository;
import com.pms.service.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

//creating RestController
@RestController

@RequestMapping("/api/v1.0/policy")
public class PolicyController
{
    //autowired the PolicyService class
    @Autowired
    PolicyService policyService;
    @Autowired
    PolicyRepository policyRepository;

    //creating a get mapping that retrieves all the policys detail from the database
    @GetMapping("/getall")
    private List<Policy> getAllPolicy()
    {
    return policyService.getAllPolicy();
    }

    //creating a get mapping that retrieves the detail of a specific policy
    @GetMapping(value="/searches", params="policyId")
    private Policy getPolicy(@RequestParam("policyId") String policyId)
    {
    return policyService.getPolicyById(policyId);
    }

    //creating a get mapping that retrieves the detail of a specific policy
    @GetMapping(value="/searches", params="policyName")
    private ResponseEntity<List<Policy>> getPolicyByPolicyName(@RequestParam("policyName") String policyName)
    {
        ResponseEntity<List<Policy>> nl = new ResponseEntity<List<Policy>>(policyRepository.findByPolicyName(policyName), HttpStatus.OK);
        if (nl.getBody().isEmpty()) System.out.println("Policies not found");
        return nl;
    }
    //creating a get mapping that retrieves the detail of a specific policy
    @GetMapping(value="/searches", params="policyType")
    private ResponseEntity<List<Policy>> getPolicyByPolicyType(@RequestParam("policyType") String policyType)
    {
        ResponseEntity<List<Policy>> nl = new ResponseEntity<List<Policy>>(policyRepository.findByPolicyType(policyType), HttpStatus.OK);
        if (nl.getBody().isEmpty()) System.out.println("Policies not found");
        return nl;
    }
    //creating a get mapping that retrieves the detail of a specific policy
    @GetMapping(value="/searches", params="companyName")
    private ResponseEntity<List<Policy>> getPolicyByCompanyName(@RequestParam("companyName") String companyName)
    {
        ResponseEntity<List<Policy>> nl = new ResponseEntity<List<Policy>>(policyRepository.findByCompanyName(companyName), HttpStatus.OK);
        if (nl.getBody().isEmpty()) System.out.println("Policies not found");
        return nl;
    }
    //creating a get mapping that retrieves the detail of a specific policy
    @GetMapping(value="/searches", params="durationInYear")
    private ResponseEntity<List<Policy>> getPolicyByDurationInYear(@RequestParam("durationInYear") Integer durationInYear)
    {
        ResponseEntity<List<Policy>> nl = new ResponseEntity<List<Policy>>(policyRepository.findByDurationInYear(durationInYear), HttpStatus.OK);
        if (nl.getBody().isEmpty()) System.out.println("Policies not found");
        return nl;
    }

    //creating a delete mapping that deletes a specific policy
    @DeleteMapping("/policy/{policyId}")
    private void deletePolicy(@PathVariable("policyId") String policyId)
    {
    policyService.delete(policyId);
    }

    //creating post mapping that post the policy detail in the database
    @PostMapping("/register")
    private String savePolicy(@RequestBody PolicyRequest policy)
    {
        Policy np = new Policy();
        np.setPolicyType(policy.getPolicyType());
        np.setPolicyName(policy.getPolicyName());
        np.setCompanyName(policy.getCompanyName());
        np.setStartDate(policy.getStartDate());
        np.setInitialDeposit(Float.parseFloat(policy.getInitialDeposit()));
        np.setDurationInYear(policy.getDurationInYear());
        np.setTermAmount(Float.parseFloat(policy.getTermAmount()));
        np.setTermsPerYear(policy.getTermsPerYear());
        np.setInterest(Float.parseFloat(policy.getInterest()));
        String newPId = createPolicyId(policy);
        np.setPolicyId(newPId);
        np.setMaturityAmount(calculateMaturityAmount(np));
        policyService.saveOrUpdate(np);
        String outStr = "Dear Admin,\n" +
                "The policy is successfully registered\n" +
                "The policy " + newPId + " is available to the users from \n" +
                np.getStartDate().toString() + " to " + np.getStartDate().plusYears(np.getDurationInYear())+
                ". This is the " + Integer.parseInt(newPId.substring(newPId.length()-3, newPId.length())) +
                "th policy in the "+ np.getPolicyType()+ " policy. To add \n" +
                "more Click <Policy Registration>.";
        System.out.println(outStr);
        return np.getPolicyId();
    }

    private Float calculateMaturityAmount(Policy np) {
        return np.getInitialDeposit()+
                (np.getDurationInYear()* np.getTermsPerYear()* np.getTermAmount())+
                (np.getDurationInYear()* np.getTermsPerYear()* np.getTermAmount())*(np.getInterest()/100);
    }

    private String createPolicyId(PolicyRequest policy) {
        List<Policy> policyList = policyRepository.findByPolicyType(policy.getPolicyType());
        if (policyList.isEmpty()) {
            return policy.getPolicyType()+ "-" +policy.getStartDate().getYear()+ "-001";
        }
        List<String> ls = policyList.stream().map(e->e.getPolicyId()).collect(Collectors.toList());
        Collections.sort(ls, Collections.reverseOrder());
        String pid = ls.get(0);
        String indStr = pid.substring(pid.length()-3, pid.length());
        int ind = Integer.parseInt(indStr);
        String ns = String.format("%03d",ind+1);
        String newPId = pid.substring(0, pid.length()-3).concat(ns);
        return newPId;
    }
}
