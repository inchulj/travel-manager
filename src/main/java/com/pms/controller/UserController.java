package com.pms.controller;

import com.pms.model.User;
import com.pms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//creating RestController
@RestController

@RequestMapping("/api/v1.0/customer")
public class UserController
{
    //autowired the UserService class
    @Autowired
    UserService userService;
    //creating a get mapping that retrieves all the users detail from the database
    @GetMapping("/user")
    private List<User> getAllUser()
    {
    return userService.getAllUser();
    }
    //creating a get mapping that retrieves the detail of a specific user
    @GetMapping("/user/{id}")
    private User getUser(@PathVariable("id") int id)
    {
    return userService.getUserById(id);
    }
    //creating a delete mapping that deletes a specific user
    @DeleteMapping("/user/{id}")
    private void deleteUser(@PathVariable("id") int id)
    {
    userService.delete(id);
    }
    //creating post mapping that post the user detail in the database
    @PostMapping("/register")
    private int saveUser(@RequestBody User user)
    {
        userService.saveOrUpdate(user);
        return user.getId();
    }
}
