package com.pms.model;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

//mark class as an Entity
@Entity
////defining class name as Table name
@Table
public class Branch {
    @Id
    private int id;
    private String name;
    private String website;
    private String contact;
    private String email;
    @ElementCollection
    private Map<String, Integer> tariff;

    public Branch() {}

    public Branch(int id, String name, String website, String contact, String email, Map<String, Integer> tariff) {
        this.id = id;
        this.name = name;
        this.website = website;
        this.contact = contact;
        this.email = email;
        this.tariff = tariff;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        email = email;
    }

    public Map<String, Integer> getTariff() {
        return this.tariff;
    }

    public void setTariff(Map<String, Integer> tariff) {
        this.tariff = tariff;
    }

    public void addTariff(String place, int tariffAmt) {
        tariff.put(place, tariffAmt);
    }

}
