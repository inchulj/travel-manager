package com.pms.model;

public enum Level {
    A(0), B(5), C(10), D(15), E(30);

    private final int level;
    private Level(int level) {
        this.level = level;
    }
    public static Level getLevel(int level) {
        Level found = A;
        for (Level l : values())
            if (l.level <= level)
                found = l;

        return found;
    }

}
