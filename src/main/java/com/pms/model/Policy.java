package com.pms.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

enum PolicyType {
    AUTO, HOME
}

//mark class as an Entity
@Entity
//defining class name as Table name
@Table
public class Policy {
    @Id
    private String policyId;
    private String policyName;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate startDate;

    private int durationInYear;
    private String companyName;
    private Float initialDeposit;
    private String policyType;
    private Level userTypes;
    private int termsPerYear;
    private Float termAmount;
    private Float interest;
    private Float maturityAmount;

    public Policy() {}

    public Policy(String policyId, String policyName, LocalDate startDate, int durationInYear,
                  String companyName,Float initialDeposit, String policyType, Level userTypes,
                  int termsPerYear, Float termAmount, Float interest, Float maturityAmount) {
        this.policyId = policyId;
        this.policyName = policyName;
        this.startDate = startDate;
        this.durationInYear = durationInYear;
        this.companyName = companyName;
        this.initialDeposit = initialDeposit;
        this.policyType = policyType;
        this.userTypes = userTypes;
        this.termsPerYear = termsPerYear;
        this.termAmount = termAmount;
        this.interest = interest;
        this.maturityAmount = maturityAmount;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public int getDurationInYear() {
        return durationInYear;
    }

    public void setDurationInYear(int durationInYear) {
        this.durationInYear = durationInYear;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Float getInitialDeposit() {
        return initialDeposit;
    }

    public void setInitialDeposit(Float initialDeposit) {
        this.initialDeposit = initialDeposit;
    }

    public String getPolicyType() {
        return policyType;
    }

    public void setPolicyType(String policyType) {
        this.policyType = policyType;
    }

    public Level getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(Level userTypes) {
        this.userTypes = userTypes;
    }

    public int getTermsPerYear() {
        return termsPerYear;
    }

    public void setTermsPerYear(int termsPerYear) {
        this.termsPerYear = termsPerYear;
    }

    public Float getTermAmount() {
        return termAmount;
    }

    public void setTermAmount(Float termAmount) {
        this.termAmount = termAmount;
    }

    public Float getInterest() {
        return interest;
    }

    public void setInterest(Float interest) {
        this.interest = interest;
    }

    public Float getMaturityAmount() { return maturityAmount; }

    public void setMaturityAmount(Float maturityAmount) { this.maturityAmount = maturityAmount; }
}