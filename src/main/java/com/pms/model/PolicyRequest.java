package com.pms.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class PolicyRequest {
    private String policyId;
    private String policyName;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate startDate;
    private int durationInYear;
    private String companyName;
    private String initialDeposit;
    private String policyType;
    private String userTypes;
    private int termsPerYear;
    private String termAmount;
    private String interest;

    public PolicyRequest() {}

    public PolicyRequest(String policyId, String policyName, LocalDate startDate, int durationInYear, String companyName, String initialDeposit, String policyType, String userTypes, int termsPerYear, String termAmount, String interest) {
        this.policyId = policyId;
        this.policyName = policyName;
        this.startDate = startDate;
        this.durationInYear = durationInYear;
        this.companyName = companyName;
        this.initialDeposit = initialDeposit;
        this.policyType = policyType;
        this.userTypes = userTypes;
        this.termsPerYear = termsPerYear;
        this.termAmount = termAmount;
        this.interest = interest;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public int getDurationInYear() {
        return durationInYear;
    }

    public void setDurationInYear(int durationInYear) {
        this.durationInYear = durationInYear;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInitialDeposit() {
        return initialDeposit;
    }

    public void setInitialDeposit(String initialDeposit) {
        this.initialDeposit = initialDeposit;
    }

    public String getPolicyType() {
        return policyType;
    }

    public void setPolicyType(String policyType) {
        this.policyType = policyType;
    }

    public String getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(String userTypes) {
        this.userTypes = userTypes;
    }

    public int getTermsPerYear() {
        return termsPerYear;
    }

    public void setTermsPerYear(int termsPerYear) {
        this.termsPerYear = termsPerYear;
    }

    public String getTermAmount() {
        return termAmount;
    }

    public void setTermAmount(String termAmount) {
        this.termAmount = termAmount;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }
}