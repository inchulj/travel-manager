package com.pms.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

enum EmployerType {SELF, SALARIED};

//mark class as an Entity
@Entity
//defining class name as Table name
@Table
public class User {
    //mark id as primary key
    @Id
//defining id as column name
    private int id;
    private String firstName;
    private String lastName;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dob;  // DD/MM/YYYY format
    private String address;
    private String contact;
    private String email;
    private String salary;
    private String pan; //- Pan Card Number
    private String employerType;
    private String employer; // Name of the Employer if Salaried or Optional

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getEmployerType() {
        return employerType;
    }

    public void setEmployerType(String employerType) {
        this.employerType = employerType;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public User() {}

    public User(Integer id, String firstName, String lastName, LocalDate dob, String address, String contact, String email, String salary, String pan, String employerType, String employer) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.address = address;
        this.contact = contact;
        this.email = email;
        this.salary = salary;
        this.pan = pan;
        this.employerType = employerType;
        this.employer = employer;
    }

    public Level getUserType(int salary) {
        return Level.getLevel(salary);
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dob=" + dob +
                ", address='" + address + '\'' +
                ", contact='" + contact + '\'' +
                ", email='" + email + '\'' +
                ", salary='" + salary + '\'' +
                ", pan='" + pan + '\'' +
                ", employerType=" + employerType +
                ", employer='" + employer + '\'' +
                '}';
    }
}
