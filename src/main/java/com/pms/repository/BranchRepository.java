package com.pms.repository;

import com.pms.model.Branch;
import com.pms.model.Policy;
import com.pms.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BranchRepository extends CrudRepository<Branch, Integer> {
    List<Branch> findByName(String name);
}
