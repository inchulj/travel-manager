package com.pms.repository;

import com.pms.model.Policy;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PolicyRepository extends CrudRepository<Policy, String>
{
    List<Policy> findByCompanyName(String companyName);
    List<Policy> findByPolicyType(String policyType);
    List<Policy> findByPolicyName(String policyName);
    List<Policy> findByDurationInYear(Integer durationInYear);
}
