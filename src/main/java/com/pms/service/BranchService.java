package com.pms.service;

import com.pms.model.Branch;
import com.pms.repository.BranchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//defining the business logic
@Service
public class BranchService
{
@Autowired
BranchRepository branchRepository;
//getting all branch records
public List<Branch> getAllBranch()
{
List<Branch> branchs = new ArrayList<Branch>();
branchRepository.findAll().forEach(branch -> branchs.add(branch));
return branchs;
}
//getting a specific record
public Branch getBranchById(int id)
{
return branchRepository.findById(id).get();
}

public void save(Branch branch)
{
branchRepository.save(branch);
}
    public void update(int id, Branch branch)
    {
        Branch nb = branchRepository.findById(id).get();
        nb = branch;
    }
//deleting a specific record
public void delete(int id) 
{
branchRepository.deleteById(id);
}
}